## 1. Getting started

### 1.1 Requirements

Before starting, make sure you have at least those components on your workstation:

- An up-to-date release of [NodeJS](https://nodejs.org/) and NPM

### 1.2 Project configuration

Start by cloning this project on your workstation.

``` sh
git clone https://gitlab.com/bohdanlevko/worthy-test/-/tree/main
```

The next thing will be to install all the dependencies of the project.

```sh
yarn install
```

Once the dependencies are installed, you can now start the project

### 1.3 Launch and discover

You are now ready to launch the application using the command below.

```sh
yarn start
```
```

You can now head to `http://localhost:3000/` and see the SPA. 

## 2. Project structure

This template was made with a well-defined directory structure.

```sh
src/
├── migrations/  # TypeORM migrations created using "npm run migration:create"
├── src
│   ├── api
│   ├── assets/  # The common assets
│   ├── components/  # A module example that manages "passenger" resources
│   │   ├── Gallery/ # Gallery component to render photos
│   │   │   ├── Gallery.tsx 
│   │   │   ├── Gallery.css  
│   │   ├── Header/  # Header component with logo and searchbar
│   │   │   ├── Header.tsx  # The model that will be returned in the response
│   │   │   ├── Header.css  # The actual TypeORM entity
│   │   ├── Modal/ # Modal component
│   │   │   ├── Modal.tsx  # The model that will be returned in the response
│   │   │   ├── Modal.css  # The actual TypeORM entity
│   │   ├── Pagination/ # Pagination component , that responsible for both pagination and infinite scroll
│   │   │   └── Pagination.tsx
│   │   │   └── Pagination.css
│   │   ├── PhotoCard/ # Photo Card component
│   │   │   └── PhotoCard.tsx
│   │   │   └── PhotoCard.css
│   └── App.tss / #Root component
│   └── App.css

```

## 3. Default NPM commands

The NPM commands below are already included with this template and can be used to quickly run, build and test your project.

```sh
# Start the application using the transpiled NodeJS
yarn start

# Transpile the TypeScript files
yarn build
```

## 4. Project goals

The goal of this project is to provide SPA for a test task.

## 5. Tech.stack

- React
- TypeScript
- React-Loader
- Framer-motion
