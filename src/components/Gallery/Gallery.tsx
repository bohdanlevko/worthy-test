import React, { Dispatch, useEffect } from "react";
import { useState } from "react";
import Pagination from "../Pagination/Pagination";
import PhotoCard, { IPhoto } from "../PhotoCard/PhotoCard";
import "./styles.css";

const Gallery: React.FC<{
  setSelectedImg: Dispatch<string>;
  data: IPhoto[] | [];
  searchedItem: string;
}> = ({ setSelectedImg, data, searchedItem }) => {
  const [dataLimit, setDataLimit] = useState(25);
  const [pageLimit, setPageLimit] = useState<number>(0);

  const changeDataLimit = (limit: number) => {
    setDataLimit(limit);
  };

  useEffect(() => {
    setPageLimit(
      data.length <= 1
        ? 0
        : data.length / dataLimit >= 5
        ? 5
        : Math.ceil(data.length / dataLimit)
    );
  }, [data, dataLimit]);

  return (
    <>
      <div className="buttonsRow">
        <button
          type="button"
          className="limitButton"
          onClick={() => changeDataLimit(20)}
        >
          20
        </button>
        <button
          type="button"
          className="limitButton"
          onClick={() => changeDataLimit(50)}
        >
          50
        </button>
        <button
          type="button"
          className="limitButton"
          onClick={() => changeDataLimit(100)}
        >
          100
        </button>
      </div>
      <Pagination
        data={data}
        pageLimit={pageLimit}
        RenderComponent={PhotoCard}
        dataLimit={dataLimit}
        setSelectedImg={setSelectedImg}
        searchedItem={searchedItem}
      />
    </>
  );
};

export default Gallery;
