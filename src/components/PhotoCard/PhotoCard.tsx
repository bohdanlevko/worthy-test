import React, { Dispatch } from "react";
import "./styles.css";

export interface IPhoto {
  albumId: number;
  id: number;
  title: string;
  url: string;
  thumbnailUrl: string;
}

const PhotoCard: React.FC<{ photo: IPhoto; setSelectedImg: Dispatch<string> }> =
  ({ photo, setSelectedImg }) => {
    return (
      <div className="photoCard">
        <img
          className="cardImage"
          src={photo.thumbnailUrl}
          alt={photo.title}
          onClick={() => setSelectedImg(photo.thumbnailUrl)}
        />
        <div className="photoTitle">{photo.title}</div>
        <div
          className="photoThumbNail"
          onClick={() => setSelectedImg(photo.thumbnailUrl)}
        >
          {photo.thumbnailUrl}
        </div>
      </div>
    );
  };

export default PhotoCard;
