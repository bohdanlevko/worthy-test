import React, {
  Dispatch,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react";
import { ReactNode } from "react";
import { IPhoto } from "../PhotoCard/PhotoCard";
import "./styles.css";

type PropsWithChildren<P> = P & { children?: ReactNode };
type ComponentProps = PropsWithChildren<{
  photo: IPhoto;
  setSelectedImg: Dispatch<string>;
}>;

const Pagination = ({
  data,
  RenderComponent,
  pageLimit,
  dataLimit,
  setSelectedImg,
  searchedItem,
}: {
  data: IPhoto[] | [];
  RenderComponent: React.FC<ComponentProps>;
  pageLimit: number;
  dataLimit: number;
  setSelectedImg: Dispatch<string>;
  searchedItem: string;
}) => {
  const [pages] = useState(Math.round(data.length / dataLimit));

  const [currentPage, setCurrentPage] = useState(1);
  const loader = useRef<HTMLHeadingElement>(null);

  const { innerWidth: width } = window;

  const goToNextPage = () => {
    setCurrentPage((page) => page + 1);
  };

  const handleObserver = useCallback((entries) => {
    const target = entries[0];
    if (target.isIntersecting) {
      setCurrentPage((prev) => prev + 1);
    }
  }, []);

  const goToPreviousPage = () => {
    setCurrentPage((page) => page - 1);
  };

  const changePage = (event: React.MouseEvent<HTMLButtonElement>) => {
    const element = event.target as HTMLElement;
    const pageNumber = Number(element.textContent);
    setCurrentPage(pageNumber);
  };

  const getPaginatedData = () => {
    const startIndex = currentPage * dataLimit - dataLimit;
    const endIndex = startIndex + dataLimit;
    return searchedItem.length >= 1
      ? data.slice(0, dataLimit)
      : data.slice(startIndex, endIndex);
  };

  const getPaginationGroup = useCallback(() => {
    let start = Math.floor((currentPage - 1) / pageLimit) * pageLimit;
    return pageLimit > 1
      ? new Array(pageLimit).fill(undefined).map((_, idx) => start + idx + 1)
      : new Array(0).fill(undefined).map((_, idx) => start + idx + 1);
  }, [currentPage, pageLimit]);

  useEffect(() => {
    if (width <= 760) {
      const option = {
        root: null,
        rootMargin: "20px",
        threshold: 0,
      };
      const observer = new IntersectionObserver(handleObserver, option);
      if (loader.current) observer.observe(loader.current);
    }
  }, [handleObserver, width, data]);

  return (
    <>
      <div className="paginated">
        {getPaginatedData().map((data: IPhoto) => (
          <RenderComponent
            setSelectedImg={setSelectedImg}
            photo={data}
            key={data.title}
          />
        ))}
      </div>
      {width <= 760 && <div ref={loader} />}
      {width >= 760 && (
        <div className="buttonsRow">
          <button
            type="button"
            onClick={goToPreviousPage}
            disabled={currentPage === 1}
            className="button"
          >
            ←
          </button>
          <div className="buttonsPaginationRow">
            {getPaginationGroup().map((item, index) => (
              <button
                key={index}
                onClick={changePage}
                className={
                  currentPage === item ? "activeButton" : "paginationButton"
                }
              >
                <span>{item}</span>
              </button>
            ))}
          </div>
          <button
            type="button"
            disabled={currentPage === pages}
            onClick={goToNextPage}
            className="button"
          >
            →
          </button>
        </div>
      )}
    </>
  );
};

export default Pagination;
