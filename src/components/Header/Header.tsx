import React from "react";

import "./styles.css";

const Header: React.FC<{
  handleChange: React.ChangeEventHandler<HTMLInputElement>;
  handleSearch: () => void;
}> = ({ handleChange, handleSearch }) => {
  return (
    <div className="header">
      <div className="title">
        <span className="titleColored">my</span>Gallery
        <span className="titleColored">.</span>
      </div>
      <div className="search">
        <input
          type="text"
          placeholder="Enter tittle or album id"
          name="search"
          className="searchField"
          onChange={handleChange}
        />
        <button className="searchButton" type="button" onClick={handleSearch}>
          Search
        </button>
      </div>
    </div>
  );
};

export default Header;
