import React, { Dispatch } from "react";
import { motion } from "framer-motion";
import "./styles.css";

const Modal: React.FC<{
  setSelectedImg: Dispatch<string | null>;
  selectedImg: string;
}> = ({ setSelectedImg, selectedImg }) => {
  const handleClick = (event: React.MouseEvent<HTMLDivElement>) => {
    const targe = event.target as HTMLDivElement;
    if (targe.classList.contains("modal")) {
      setSelectedImg(null);
    }
  };
  return (
    <motion.div
      className="modal"
      onClick={handleClick}
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
    >
      <motion.img
        src={selectedImg}
        className="modalImage"
        alt="enlarged pic"
        initial={{ y: "-100vh" }}
        animate={{ y: "20vh" }}
      />
    </motion.div>
  );
};

export default Modal;
