export const fetchPhotos = (setInitialData, setData, url) => {
  fetch(url)
    .then((response) => {
      if (response.ok) return response.json();
      throw new Error("something went wrong while requesting posts");
    })
    .then((data) => {
      setInitialData(data);
      setData(data);
    })
    .catch((error) => console.log(error));
};
