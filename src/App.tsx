import { useState, useEffect } from "react";
import { fetchPhotos } from "./api/fetchPhotos";
import "./App.css";
import Gallery from "./components/Gallery/Gallery";
import Header from "./components/Header/Header";
import Loader from "react-loader";
import Modal from "./components/Modal/Modal";
import { IPhoto } from "./components/PhotoCard/PhotoCard";
import notFound from "./assets/gif/notFound.gif";

function App() {
  const [selectedImg, setSelectedImg] = useState<string | null>(null);
  const [searchedItem, setSearchedItem] = useState<string>("");
  const [initialData, setInitialData] = useState<IPhoto[] | null>([]);
  const [data, setData] = useState<IPhoto[] | []>([]);
  const [loaded, setLoaded] = useState(false);

  const handleSearchInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchedItem(event.target.value);
  };

  const numberRegexp = new RegExp("^-?\\d*(\\.\\d+)?$");

  const renderElement = () => {
    if (loaded) {
      return data.length >= 1 ? (
        <Gallery
          setSelectedImg={setSelectedImg}
          data={data}
          searchedItem={searchedItem}
        />
      ) : (
        <div className="notFound">
          <div className="notFoundText">
            Sorry, no photos found, please try again later!
          </div>
          <img
            src={notFound}
            alt="no photos to display"
            className="notFoundImage"
          />
        </div>
      );
    }
  };

  const handleSearch = () => {
    if (initialData) {
      setData(
        [...initialData].filter((data: IPhoto) => {
          if (numberRegexp.test(searchedItem)) {
            return data.id === Number(searchedItem);
          } else {
            return data.title
              .toLocaleLowerCase()
              .includes(searchedItem.toLocaleLowerCase());
          }
        })
      );
    }
  };

  useEffect(() => {
    fetchPhotos(
      setInitialData,
      setData,
      "https://jsonplaceholder.typicode.com/photos/"
    );
    setTimeout(() => {
      setLoaded(true);
    }, 500);
  }, []);

  return (
    <div className="App">
      <Header handleChange={handleSearchInput} handleSearch={handleSearch} />
      {!loaded ? (
        <Loader
          loaded={loaded}
          lines={13}
          length={20}
          width={10}
          radius={30}
          corners={1}
          rotate={0}
          direction={1}
          color="#000"
          speed={1}
          trail={60}
          shadow={false}
          hwaccel={false}
          className="spinner"
          zIndex={2e9}
          top="50%"
          left="50%"
          scale={1.0}
          loadedClassName="loadedContent"
        />
      ) : (
        renderElement()
      )}
      {selectedImg && (
        <Modal selectedImg={selectedImg} setSelectedImg={setSelectedImg} />
      )}
    </div>
  );
}

export default App;
